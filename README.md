# CML with cloud compute


This repository contains a sample project using [CML](https://github.com/iterative/cml) with Terraform (via the `cml-runner` function) to launch an AWS EC2 instance and then run a neural style transfer on that instance. On a pull request, the following actions will occur:
- GitLab will deploy a runner with a custom CML Docker image
- `cml-runner` will provision an EC2 instance and pass the neural style transfer workflow to it. DVC is used to version the workflow and dependencies. 
- Neural style transfer will be executed on the EC2 instance 
- CML will report results of the style transfer as a comment in the pull request. 

The key file enabling these actions is `.gitlab-ci.yml`.

## CI/CD variables
⚠️ Note that in addition to providing your AWS credentials as variables in GitLab CI/CD, you must create a variable called `repo_token`. 

1. Create a Personal Access Token by navigating to User Settings → Access Tokens.

2. In the "Name" field, type "repo_token" and check boxes to select "api", "read_repository" and "write_repository". Then click "Create personal access token".

3. Copy the generated Personal Access Token.

4. Click "Add Variable". In the Key field, type "repo_token". 
5. In the Value field, paste your Personal Access Token. Check the "Mask variable" box, uncheck "Protect variable", and then save the variable by clicking "Add variable" at the bottom of the dialog box.

